from flask import Flask,render_template
import os, requests
from dotenv import load_dotenv
load_dotenv()

app = Flask(__name__)

@app.route('/', methods=["GET"])
def home():
   return render_template('home.html')

# @app.route('/')
# def index():
#     return 'home'




def get_weather_data(location='Toronto'):

    API_KEY = os.environ.get("API_KEY")

    # BASE_URL = 'http://api.weatherstack.com/current'
    BASE_URL = 'http://api.weatherapi.com/v1/current.json?key=bfb8beef88dd4f97b05150411241306&q=Toronto&aqi=no'

    response = requests.get(BASE_URL, params={

        'access_key': API_KEY,

        'query': location

    })

    return response.json()




@app.route('/dashboard')
def hello():
        # openweather = requests.get("http://api.weatherapi.com/v1/current.json?key={}|&q=Toronto&aqi=no".format(os.environ.get("API_KEY")))
        openweather = get_weather_data()
    
        print(openweather)
        return render_template('dashboard.html', weather=openweather)

